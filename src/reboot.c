/*
Copyright (C) 2015, David "Davee" Morgan

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

*/

#include "reboot.h"
#include "utility.h"
#include "psp_uart.h"
#include "kprintf.h"

#include <pspmacro.h>
#include <config.h>

#include <pspkernel.h>
#include <pspsysmem_kernel.h>
#include <psploadcore.h>

#include <stdio.h>
#include <string.h>

#define SONY_REBOOT_MODULE_ADDR     (0x88600000)
#define REBOOTEX_MODULE_ADDR        (0x88F10000)
#define COMPATIBILITY_MODULE_ADDR   (0x88F50000)
#define TEMP_REBOOT_MODULE_ADDR     (0x88FB0000)

int (* g_rebootex_entry)(SceKernelBootParam *reboot_param, SceLoadExecInternalParam *exec_param, int api, int initial_rnd, int have_compat, int bootloader_version) = (void *)REBOOTEX_MODULE_ADDR;
int (* g_sony_reboot)(SceKernelBootParam *reboot_param, SceLoadExecInternalParam *exec_param, int api, int initial_rnd) = (void *)SONY_REBOOT_MODULE_ADDR;

int sceKernelExitVSHKernel(void *param);

int g_reboot_size = 0, g_has_compat = 0;
char *g_reboot_ptr = (char *)TEMP_REBOOT_MODULE_ADDR;
char *g_sony_entry = (char *)SONY_REBOOT_MODULE_ADDR;

static int get_compatibility_module(char *data)
{
    char compat_path[256];

    // we need to load our configuration handler
    int res = LoadStartModule("flash0:/kd/libconfig631.prx");

    if (res < 0)
    {
        return res;
    }

     // get our config
    res = infInitConfig(INFINITY_CONFIGURATION_LOCATION);

    if (res < 0)
    {
        return res;
    }

    // read the path to the compatibility module
    res = infGetEntry("inf_boot_compat_path", compat_path, sizeof(compat_path));

    if (res < 0)
    {
        return res;
    }

    // read in the compat module from wherever it is located
    res = ReadFile(compat_path, 0, data, 0x20000);

    if (res < 0)
    {
        return res;
    }

    return 0;
}

int RebootStartPatched(SceKernelBootParam *reboot_param, SceLoadExecInternalParam *exec_param, int api, int initial_rnd)
{
    // overwrite reboot.bin with 6.61 version
    memcpy(g_sony_entry, g_reboot_ptr, g_reboot_size);

    psp_uart_init(115200);
    kprintf("entering 6.61 rebootex!\n");

    g_rebootex_entry(reboot_param, exec_param, api, initial_rnd, g_has_compat, GIT_COMMIT_VERSION);
    return g_sony_reboot(reboot_param, exec_param, api, initial_rnd);
}

int reboot_661(SceSize args, void *argp)
{
    char reboot_path[128];

    // wait for init to shutdown before we proceed
    while (sceKernelFindModuleByName("sceInit") != NULL)
    {
        sceKernelDelayThread(100);
    }

    sprintf(reboot_path, "flash0:/kn/reboot_%02ig.bin", sceKernelGetModel()+1);

    // wait for flash0 ready
    SceUID fd = -1;
    while ((fd = sceIoOpen(reboot_path, PSP_O_RDONLY, 0777)) == 0x80020321)
    {
        sceKernelDelayThread(100);
    }

    if (fd >= 0)
    {
        sceIoClose(fd);
    }

    // delay for screen activation
    sceKernelDelayThread(500*1000);

    // load any compatibility modules registered
    g_has_compat = (get_compatibility_module((void *)COMPATIBILITY_MODULE_ADDR) >= 0);

    // read in 6.61 reboot.bin for the correct model
    g_reboot_size = ReadFile(reboot_path, 0, g_reboot_ptr, 2*1024*1024);

    // read in infinity rebootex for 6.61
    ReadFile("flash0:/kn/rebootex661.bin", 0, g_rebootex_entry, 0x40000);

    // try to reboot indefinitely
    while (1)
    {
        sceKernelExitVSHKernel(NULL);
        sceKernelDelayThread(500*1000);
    }

    return 0;
}
