/*

Copyright (C) 2015, David "Davee" Morgan

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.


 */

#include "reboot.h"
#include "psp_uart.h"
#include "kprintf.h"

#include <pspmacro.h>

#include <pspkernel.h>
#include <psploadcore.h>
#include <pspsysmem_kernel.h>
#include <pspctrl.h>

#include <stdio.h>
#include <string.h>

PSP_MODULE_INFO("Carbon", 0x1007, 1, 0);

#define M33_KERNEL_EXEC_TAG			(0x55668D96)

volatile u8 g_init_prx_data[5776] __attribute__((aligned(64))) =
{
    'p', 's', 'a', 'r', '0', ':', '/', 'f', 'l', 'a', 's', 'h', '0', '/', 'k', 'd', '/', 'i', 'n', 'i', 't', '.', 'p', 'r', 'x', 0
};

volatile u8 g_systimer_prx_data[2272] __attribute__((aligned(64))) =
{
    'p', 's', 'a', 'r', '0', ':', '/', 'f', 'l', 'a', 's', 'h', '0', '/', 'k', 'd', '/', 's', 'y', 's', 't', 'i', 'm', 'e', 'r', '.', 'p', 'r', 'x', 0
};

int (* PrologueModule)(void *modmgr_param, SceModule2 *mod) = (void *)NULL;
int (* DecryptExecutable)(PSP_Header *exec_data, u32 size, u32 *outsize) = NULL;
int (* UnSigncheck)(PSP_Header *exec_data, u32 size, u32 *outsize) = NULL;

void ClearCaches(void)
{
	sceKernelIcacheClearAll();
	sceKernelDcacheWritebackAll();
}

SceKernelBootParam *g_boot_param;

int recovery_thread(SceSize args, void *argp)
{
    while (sceKernelFindModuleByName("sceInit") != NULL)
    {
        sceKernelDelayThread(100);
    }

    kprintf("init prx unloaded, starting recovery.\n");

    SceUID modid = -1;
    while ((modid = sceKernelLoadModule("ms0:/infinity/recovery.prx", 0, NULL)) == 0x80020321)
    {
        sceKernelDelayThread(100);
    }

    if (modid < 0)
    {
        kprintf("error 0x%08X loading recovery.prx!\n", modid);
        return 1;
    }

    int res = sceKernelStartModule(modid, 0, NULL, NULL, NULL);

    if (res < 0)
    {
        kprintf("error 0x%08X starting recovery.prx!\n", res);
        return 1;
    }

    while (1)
    {
        kprintf("recovery running!\n");
        sceKernelDelayThread(1*1000*1000);
    }

    return 0;
}

int UnSigncheckPatched(PSP_Header *exec_data, u32 size, u32 *outsize)
{
	int i;

	/* If it's not ~PSP we will return success */
	if (exec_data->signature != 0x5053507E)
	{
		/* return success */
		return 0;
	}

	/* Or these values together and use signcheck_check as a boolean value */
	u32 signcheck_check =  exec_data->reserved2[0] | exec_data->reserved2[1];

	if (signcheck_check)
	{
		for (i = 0; i < 0x58; i++)
		{
			if (exec_data->scheck[i])
			{
				/* Executable is signchecked, we better pass it through the original function to remove it */
				return UnSigncheck(exec_data, size, outsize);
			}
		}
	}

    // not signchecked, succeed anyway
	return 0;
}

int DecryptExecutablePatched(PSP_Header *exec_data, u32 size, u32 *outsize)
{
    // check for M33 ~PSP packed exec
	if (exec_data && outsize && exec_data->oe_tag == M33_KERNEL_EXEC_TAG)
	{
        // check for gzip header
        if (((u8 *)exec_data)[0x150] == 0x1F && (((u8 *)exec_data)[0x151] == 0x8B))
        {
            // move gzip data to front of buffer
            *(int *)outsize = ((u32 *)exec_data)[0xB0/4];

            memmove((void *)exec_data, (void *)exec_data+0x150, *(int *)outsize);
            return 0;
        }
	}

	return DecryptExecutable(exec_data, size, outsize);
}

int PrologueModulePatched(void *modmgr_param, SceModule2 *mod)
{
    u32 text_addr = mod->text_addr;

	// modmgr_param has changed from 1.50 so I have not included the structure definition, for an updated version a re-reverse of 6.20+ modulemgr is required
	int res = PrologueModule(modmgr_param, mod);

	// If this function errors, the module is shutdown so we better check for it
	if (res >= 0)
	{
		if (strcmp(mod->modname, "sceLoadExec") == 0)
		{
            switch (sceKernelGetModel())
            {
            case PSP_MODEL_PHAT:
            case PSP_MODEL_SLIM:
            case PSP_MODEL_BRITE:
            case PSP_MODEL_BRITE4G:
            case PSP_MODEL_BRITE7G:
            case PSP_MODEL_BRITE9G:
                // Patch $k1 and userlevel checks for kernel mode to allow pure kernel execution of sceKernelLoadExecVSH* api
                MAKE_RELATIVE_BRANCH(0x168C, 0x16C8, text_addr); //6.31
                MAKE_RELATIVE_BRANCH(0x23B8, 0x2404, text_addr); //6.31
                MAKE_RELATIVE_BRANCH(0x25BC, 0x260C, text_addr); //6.31
                MAKE_CALL(mod->text_addr + 0x2D94, RebootStartPatched); //6.31
                break;

            case PSP_MODEL_PSPGO:
                MAKE_RELATIVE_BRANCH(0x168C, 0x16C8, text_addr); //6.31
                MAKE_RELATIVE_BRANCH(0x260C, 0x2658, text_addr); //6.31
                MAKE_RELATIVE_BRANCH(0x2810, 0x2860, text_addr); //6.31
                MAKE_CALL(mod->text_addr + 0x2FE0, RebootStartPatched); //6.31
                break;

            default:
                kprintf("unsupported PSP model! %02ig\n", sceKernelGetModel()+1);
                break;
            }

            ClearCaches();
		}

        // ideally we want to check for L trigger a lot earlier than the MS drivers
        // for now we will just check with ctrl.prx but in future will be syscon
        else if ((strcmp(mod->modname, "sceFATFS_Driver") == 0)
                || (strcmp(mod->modname, "sceMSFAT_Driver") == 0))
        {
            SceCtrlData pad;
            sceCtrlPeekBufferPositive(&pad, 1);

            if (pad.Buttons & PSP_CTRL_LTRIGGER)
            {
                kprintf("Entering recovery mode\n");

                // force init.prx to halt
                g_boot_param->cur_modent_n = g_boot_param->modentry_n;

                SceUID thid = sceKernelCreateThread("recovery", &recovery_thread, 0x16, 0x4000, 0, NULL);

                if (thid < 0)
                {
                    kprintf("error creating recovery thread...!!\n");
                }

                sceKernelStartThread(thid, 0, NULL);
            }
            else
            {
                // force init.prx to halt
                g_boot_param->cur_modent_n = g_boot_param->modentry_n;

                SceUID thid = sceKernelCreateThread("reboot661", &reboot_661, 0x16, 0x4000, 0, NULL);

                if (thid < 0)
                {
                    kprintf("error creating reboot661 thread...!!\n");
                }

                sceKernelStartThread(thid, 0, NULL);
            }
        }

        kprintf("loaded modules 0x%08X out of 0x%08X\n", g_boot_param->cur_modent_n, g_boot_param->modentry_n);
        kprintf("module starting: %s\n", mod->modname);
	}

	/* return success */
	return res;
}

int sceIoIoctlPatched(SceUID fd, u32 cmd, void *indata, int inlen, void *outdata, int outlen)
{
	// lets check for a few commands
	if (cmd == 0x208081)
	{
		// this command is checking the device, force return success (as to store a "non secure?" device in the exec_info)
		return 0;
	}

	// check for a loadmodule command but avoid the signcheck check
	else if ((cmd & 0x208080) == 0x208000)
	{
		// force a successful return (is checking if it's a loadmodule device, would return 0x80020146)
		return 0;
	}

	return sceIoIoctl(fd, cmd, indata, inlen, outdata, outlen);
}

void PatchModuleManager(void)
{
	/* get the module structure of modulemgr */
	SceModule2 *mod = sceKernelFindModuleByName("sceModuleManager");
    u32 text_addr = mod->text_addr;

    /* link prologue module */
    PrologueModule = (void *)(text_addr + 0x8138); //6.31

    /* Patch call to PrologueModule from the StartModule function to allow a full coverage of loaded modules (even those without an entry point) */
    MAKE_CALL(text_addr + 0x705C, PrologueModulePatched); //6.31

    // patch sceIoIoctl import to allow homebrew from ef0/ms0
    MAKE_JUMP(text_addr + 0x87B0, sceIoIoctlPatched); //6.31
}

void PatchLoadCore(void)
{
	/* perform patches on the loadcore */
	SceModule2 *mod = sceKernelFindModuleByName("sceLoaderCore");
	u32 text_addr = mod->text_addr;

	DecryptExecutable = (void *)(text_addr + 0x8398); //6.31
	UnSigncheck = (void *)(text_addr + 0x8378); //6.31

	MAKE_CALL(text_addr + 0x6930, DecryptExecutablePatched); //6.31
	MAKE_CALL(text_addr + 0x6954, UnSigncheckPatched); //6.31
}

int module_start(SceKernelBootParam *boot_param, u32 *cur_mod_n)
{
    psp_uart_init(115200);
    kprintf("6.31 bootloader entered.\n");

	/* move the mod count back to fit in new modules (to fit in systimer and init.prx) */
	cur_mod_n[0] -= 1;

	/* get pointers to the modules */
	PSP_Header *init_prx = (PSP_Header *)g_init_prx_data;
	PSP_Header *systimer_prx = (PSP_Header *)g_systimer_prx_data;

	/* now set the current entry to init */
	SceInitBootModuleEntry *init = &boot_param->module_entry[cur_mod_n[0] + 1];

	/* store init.prx info */
	init->module_buffer = init_prx;
	init->module_size = init_prx->psp_size;
	init->flags |= SCE_INIT_MODULE_SIGNCHECK;

	/* now set the current entry to systimer */
	SceInitBootModuleEntry *systimer = &boot_param->module_entry[cur_mod_n[0]];

	/* store systimer.prx info */
	systimer->module_buffer = systimer_prx;
	systimer->module_size = systimer_prx->psp_size;
	systimer->flags |= SCE_INIT_MODULE_SIGNCHECK;

	/* move the mod count back again to defeat the counter increment (to fit in systimer and init.prx) */
    cur_mod_n[0] -= 1;

    /* patch modulemgr to detect startup of ms0 */
    PatchModuleManager();
    PatchLoadCore();

    g_boot_param = boot_param;

    /* clear caches */
    ClearCaches();

	return 0;
}
