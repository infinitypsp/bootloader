release: all
	psp-fixup-imports -m $(PSP_FW_VERSION)mapfile.txt $(TARGET).prx

TARGET = bootloader
OBJS = src/main.o src/reboot.o src/utility.o src/kprintf.o src/psp_uart.o libasm/libconfig.o libasm/sceCtrl_driver.o libasm/LoadExecForKernel.o

GIT_VERSION := $(shell git rev-list HEAD --count)

INCDIR = include
CFLAGS = -Os -G0 -Wall -fno-pic -fshort-wchar -DGIT_COMMIT_VERSION=$(GIT_VERSION)
ASFLAGS = $(CFLAGS)

BUILD_PRX = 1
PRX_EXPORTS = src/exports.exp

PSP_FW_VERSION = 631

USE_KERNEL_LIBS=1
USE_KERNEL_LIBC=1

LIBDIR = ../lib
LDFLAGS = -nostartfiles
LIBS =

PSPSDK=$(shell psp-config --pspsdk-path)
include $(PSPSDK)/lib/build.mak
